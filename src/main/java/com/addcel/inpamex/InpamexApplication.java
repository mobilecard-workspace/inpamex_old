package com.addcel.inpamex;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.addcel.inpamex.data.dao.AppAuthorizationDao;
import com.addcel.inpamex.data.entity.AppAuthorization;
import com.addcel.inpamex.dto.AppUser;

@SpringBootApplication
@ComponentScan(basePackages = "com.addcel.inpamex")
public class InpamexApplication extends SpringBootServletInitializer {
	
	private final static Logger logger = Logger.getLogger(InpamexApplication.class);
	
	@Value("${app.auth.id}")
	private String appId;

	@Value("${app.auth.status}")
	private String appStatus;
	
	@Autowired
	private AppAuthorizationDao appAuthorizationDao;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InpamexApplication.class);
	}
	
	@Bean
	@PostConstruct
	public AppUser appUser() {
		AppUser appUser = new AppUser();
		try {
			Optional<AppAuthorization> auth = appAuthorizationDao.findByIdApplicationAndActivo(appId, appStatus);
			if (auth != null & auth.isPresent()) {
				appUser.setUsername(auth.get().getUsername());
				appUser.setPassword(auth.get().getPassword());
			} else {
				logger.error("NO ES POSIBLE OBTENER LOS DATOS DE AUTENTICACION PARA MOBILE CARD");
			}
		} catch (Exception e) {
			logger.error("ERROR AL OBTENER LA AUTENTICACION DESDE LA BD " + e.getLocalizedMessage());
		}
		return appUser;
	}

	public static void main(String[] args) {
		SpringApplication.run(InpamexApplication.class, args);
	}

}
