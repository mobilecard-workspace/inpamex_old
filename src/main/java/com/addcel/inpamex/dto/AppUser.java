package com.addcel.inpamex.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppUser {
	
	private String username;
	private String password;

}
